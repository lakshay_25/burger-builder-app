import React from "react";

import BurgerMaker from "./BurgerMaker";
import ToppingsMenu from "./ToppingsMenu";
import { useSelector, useDispatch } from "react-redux";
import { tomatoCounterActions } from "../store/tomato";
import { lettuceCounterActions } from "../store/lettuce";
import { cheeseCounterActions } from "../store/cheese";
import { meatCounterActions } from "../store/meat";

function Burger() {
  // const [tomatoQuantity, setTomatoQuantity] = useState(0);
  // const [lettuceQuantity, setlettuceQuantity] = useState(0);
  // const [cheeseQuantity, setCheeseQuantity] = useState(0);
  // const [meatQuantity, setMeatQuantity] = useState(0);

  const tomatoQuantity = useSelector((state) => state.tomato.counter);
  const lettuceQuantity = useSelector((state) => state.lettuce.counter);
  const cheeseQuantity = useSelector((state) => state.cheese.counter);
  const meatQuantity = useSelector((state) => state.meat.counter);
  const dispatch = useDispatch();

  const allToppings = [
    {
      type: "tomato",
      onAdd() {
        // setTomatoQuantity((prevState) => prevState + 1);
        dispatch(tomatoCounterActions.onAdd());
      },
      onRemove() {
        if (tomatoQuantity === 0) {
          return;
        }
        // setTomatoQuantity((prevState) => prevState - 1);
        dispatch(tomatoCounterActions.onRemove());
      },
    },
    {
      type: "lettuce",
      onAdd() {
        // setlettuceQuantity((prevState) => prevState + 1);
        dispatch(lettuceCounterActions.onAdd());
      },
      onRemove() {
        if (lettuceQuantity === 0) {
          return;
        }
        // setlettuceQuantity((prevState) => prevState - 1);
        dispatch(lettuceCounterActions.onRemove());
      },
    },
    {
      type: "cheese",
      onAdd() {
        // setCheeseQuantity((prevState) => prevState + 1);
        dispatch(cheeseCounterActions.onAdd());
      },
      onRemove() {
        if (cheeseQuantity === 0) {
          return;
        }
        // setCheeseQuantity((prevState) => prevState - 1);
        dispatch(cheeseCounterActions.onRemove());
      },
    },
    {
      type: "meat",
      onAdd() {
        // setMeatQuantity((prevState) => prevState + 1);
        dispatch(meatCounterActions.onAdd());
      },
      onRemove() {
        if (meatQuantity === 0) {
          return;
        }
        // setMeatQuantity((prevState) => prevState - 1);
        dispatch(meatCounterActions.onRemove());
      },
    },
  ];

  return (
    <React.Fragment>
      <BurgerMaker
        tomatoQuantity={tomatoQuantity}
        lettuceQuantity={lettuceQuantity}
        cheeseQuantity={cheeseQuantity}
        meatQuantity={meatQuantity}
      />
      <ToppingsMenu allToppings={allToppings} />
    </React.Fragment>
  );
}

export default Burger;
