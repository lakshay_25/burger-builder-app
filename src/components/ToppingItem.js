import classes from "./Toppings.module.css";

function Toppings(props) {
  return (
    <div className={classes["ingredients-block"]}>
      <p>{props.toppingName}</p>
      <div className={classes["ingr-btns"]}>
        <button className={classes["ingr-btn"]} onClick={props.onAdd}>
          Add
        </button>
        <button className={classes["ingr-btn"]} onClick={props.onRemove}>
          Remove
        </button>
      </div>
    </div>
  );
}

export default Toppings;
