import classes from "./Toppings.module.css";

function Toppings(props) {
  return (
    <div className={classes.ingredientsBlock}>
      <p>{props.toppingName}</p>
      <div className={classes.ingrBtns}>
        <button className={classes.ingrBtn}>Add</button>
        <button className={classes.ingrBtn}>Remove</button>
      </div>
    </div>
  );
}

export default Toppings;
