import React from "react";
import ToppingItem from "./ToppingItem";

function ToppingsMenu(props) {
  const toppingsList = props.allToppings.map((topping) => (
    <ToppingItem
      key={Math.random()}
      toppingName={topping.type}
      onAdd={topping.onAdd}
      onRemove={topping.onRemove}
    />
  ));

  return <React.Fragment>{toppingsList}</React.Fragment>;
}

export default ToppingsMenu;
