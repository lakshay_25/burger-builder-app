import { Fragment } from "react";
import burgerTop from "../assests/top.jpg";
import burgerBottom from "../assests/bottom.jpg";
import tomato from "../assests/tomato.jpg";
import cheese from "../assests/cheese.jpg";
import meat from "../assests/meat.jpg";
import lettuce from "../assests/lettuce.jpg";

function BurgerMaker(props) {
  const { tomatoQuantity, lettuceQuantity, cheeseQuantity, meatQuantity } =
    props;
  const renderToppings = (itemQuantity, itemPath) => {
    const itemsArray = [];
    for (let i = 0; i < itemQuantity; i++) {
      itemsArray.push(
        <div key={Math.random()}>
          <img src={itemPath} alt="" />
        </div>
      );
    }
    return itemsArray;
  };

  const tomatoToppingQuantity = renderToppings(tomatoQuantity, tomato);
  const lettuceToppingQuantity = renderToppings(lettuceQuantity, lettuce);
  const cheeseToppingQuantity = renderToppings(cheeseQuantity, cheese);
  const meatToppingQuantity = renderToppings(meatQuantity, meat);

  const noToppings =
    !tomatoToppingQuantity.length &&
    !lettuceToppingQuantity.length &&
    !cheeseToppingQuantity.length &&
    !meatToppingQuantity.length;

  return (
    <Fragment>
      <div>
        <img src={burgerTop} alt="Burger Top" />
      </div>
      {noToppings ? (
        <p>Please add some toppings to your burger</p>
      ) : (
        <div>
          {tomatoToppingQuantity.map((topping) => topping)}
          {lettuceToppingQuantity.map((topping) => topping)}
          {cheeseToppingQuantity.map((topping) => topping)}
          {meatToppingQuantity.map((topping) => topping)}
        </div>
      )}
      <div>
        <img src={burgerBottom} alt="Burger Bottom" />
      </div>
    </Fragment>
  );
}

export default BurgerMaker;
