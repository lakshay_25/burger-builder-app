import {createSlice} from "@reduxjs/toolkit";

const initialState = {counter : 0}

const meatSlice = createSlice({
    name : "meat",
    initialState,
    reducers : {
        onAdd(state){
            state.counter++;
        },
        onRemove(state){
            state.counter--;
        }
    }  
});

export const meatCounterActions = meatSlice.actions;

export default meatSlice.reducer;