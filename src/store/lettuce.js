import {createSlice} from "@reduxjs/toolkit";

const initialState = {counter : 0}

const lettuceSlice = createSlice({
    name : "lettuce",
    initialState,
    reducers : {
        onAdd(state){
            state.counter++;
        },
        onRemove(state){
            state.counter--;
        }
    }  
});

export const lettuceCounterActions = lettuceSlice.actions;

export default lettuceSlice.reducer;