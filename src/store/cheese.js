import {createSlice} from "@reduxjs/toolkit";

const initialState = {counter : 0}

const cheeseSlice = createSlice({
    name : "cheese",
    initialState,
    reducers : {
        onAdd(state){
            state.counter++;
        },
        onRemove(state){
            state.counter--;
        }
    }  
});

export const cheeseCounterActions = cheeseSlice.actions;

export default cheeseSlice.reducer;