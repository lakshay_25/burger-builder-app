import {createSlice} from "@reduxjs/toolkit";

const initialState = {counter : 0}

const tomatoSlice = createSlice({
    name : "tomato",
    initialState,
    reducers : {
        onAdd(state){
            state.counter++;
        },
        onRemove(state){
            state.counter--;
        }
    }  
});

export const tomatoCounterActions = tomatoSlice.actions;

export default tomatoSlice.reducer;