import { configureStore } from "@reduxjs/toolkit";
import tomatoReducer from './tomato';
import cheeseReducer from './cheese';
import lettuceReducer from './lettuce';
import meatReducer from './meat';

const store = configureStore({
    reducer : {
        tomato : tomatoReducer,
        cheese : cheeseReducer,
        lettuce : lettuceReducer,
        meat : meatReducer
    }   
});

export default store;